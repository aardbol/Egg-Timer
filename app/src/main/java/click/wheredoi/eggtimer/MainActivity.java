package click.wheredoi.eggtimer;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {
    SeekBar seekBarTime;
    TextView textViewTime;
    Button buttonTrigger;
    Integer timeLeft = 0;
    boolean isRunning = false;
    CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonTrigger = findViewById(R.id.buttonTrigger);
        textViewTime = findViewById(R.id.textViewTime);
        seekBarTime = findViewById(R.id.seekBarTime);
        seekBarTime.setMax(300);
        seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                timeLeft = i;
                textViewTime.setText(fromIntToHmm(timeLeft));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
    }

    public String fromIntToHmm(int minutes) {
        long hours = TimeUnit.MINUTES.toHours(Long.valueOf(minutes));
        long remainMinutes = minutes - TimeUnit.HOURS.toMinutes(hours);
        return String.format("%2d:%02d", hours, remainMinutes);
    }

    public void startStop(View view) {
        if (!isRunning && timeLeft != 0) {
            seekBarTime.setEnabled(false);
            buttonTrigger.setText("STOP");

            timer = new CountDownTimer(timeLeft * 1000 + 100, 1000) {

                @Override
                public void onTick(long l) {
                    l /= 1000;
                    timeLeft = (int) l;
                    textViewTime.setText(fromIntToHmm(timeLeft));
                }

                @Override
                public void onFinish() {
                    Toast.makeText(getApplicationContext(), "Time's up!", Toast.LENGTH_SHORT).show();
                    Log.i("time", "Time's up!");
                    timeLeft = 0;
                    MediaPlayer.create(getApplicationContext(), R.raw.moo).start();
                    stop();
                }
            };

            timer.start();
            isRunning = true;
        } else if (timeLeft != 0) {
            stop();
        }
    }

    protected void stop() {
        timer.cancel();
        seekBarTime.setEnabled(true);
        seekBarTime.setProgress(timeLeft);
        buttonTrigger.setText("START");
        isRunning = false;
    }
}
